using System;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity {

    public class Query {
        /// <summary>
        /// Gets all students.
        /// </summary>
        [UsePaging]
        [UseSelection]
        [UseFiltering]
        [UseSorting]
        public IQueryable<Student> GetStudents([Service] SchoolContext context) =>
         context.Students.AsQueryable();


        /// <summary>
        /// Gets all courses.
        /// </summary>
        [UsePaging]
        [UseSelection]
        [UseFiltering]
        [UseSorting]
        public IQueryable<Course> GetCourses([Service] SchoolContext context) =>
          context.Courses.AsQueryable();

        [UseFirstOrDefault]
        [UseSelection]
        public IQueryable<Student> GetStudentById([Service] SchoolContext context, int studentId) =>
          context.Students.Where(t => t.Id == studentId);


        [UseFirstOrDefault]
        [UseSelection]
        public IQueryable<Course> GetCourseById([Service] SchoolContext context1, int courseId) =>
          context1.Courses.Where(t => t.CourseId == courseId);
    }
}